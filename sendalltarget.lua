addon.name      = 'sendalltarget'
addon.author    = 'Selindrile (ported to Ashita by Bambooya)'
addon.version   = '1.1'
addon.desc      = 'commands all or subset of characters to target, switch target, attack, etc. the current player\'s target'
addon.link      = 'https://gitlab.com/ffxi_multibox/ashita/addons/sendalltarget'

require('common')

-- This addon requires the Multisend plugin by Thorny: https://github.com/ThornyFFXI/Multisend

local function get_target()
	local target = AshitaCore:GetMemoryManager():GetTarget()
	local entity = AshitaCore:GetMemoryManager():GetEntity()
	local target_index = target:GetTargetIndex(0) -- 0: current target/subtarget
	if target:GetIsSubTargetActive() ~= 0 then
		target_index = target:GetTargetIndex(1) -- 1: target when subtarget is active
	end
	local target_id = entity:GetServerId(target_index)
	if target_id > 0 then
		return T{
			id=target_id,
			index=target_index,
		}
	else
		return nil
	end
end

local function get_player()
	local player = GetPlayerEntity()
	if player ~= nil then
		return T{
			id=player.ServerId,
			index=player.TargetIndex,
		}
	else
		return nil
	end
end

local function get_target_by_id(id)
	for index = 0, 2303 do
        local e = GetEntity(index)
        if e ~= nil and e.ServerId ~= nil and e.ServerId == id then
            return T{
				id=id,
				index=e.TargetIndex,
				spawn_type=e.SpawnFlags,
				valid_target=(bit.band(e.SpawnFlags, 0x10) > 0),
				distance=e.Distance,
			}
        end
    end
    return nil
end

ashita.events.register('load', 'sat_load_callback', function ()
	if not AshitaCore:GetPluginManager():IsLoaded('Multisend') then
		print('Warning: sendalltarget addon requires Multisend plugin - https://github.com/ThornyFFXI/Multisend')
	end
end)

ashita.events.register('command', 'sat_command_callback', function (e)
    local args = e.command:args()
    local command = table.remove(args, 1)
    if command ~= '/sendalltarget' and command ~= '/sendallt' and command ~= '/sendat' and command ~= '/sat' then return end
	if not AshitaCore:GetPluginManager():IsLoaded('Multisend') then
		print('Warning: sendalltarget addon requires Multisend plugin - https://github.com/ThornyFFXI/Multisend')
		return
	end
    local subcommand = table.remove(args, 1)
    local subcommand2 = #args > 0 and table.remove(args, 1) or nil
	if subcommand == 'stop' or subcommand == 'unfollow' then
		AshitaCore:GetMemoryManager():GetAutoFollow():SetFollowTargetServerId(0)
    	AshitaCore:GetMemoryManager():GetAutoFollow():SetFollowTargetIndex(0)
		AshitaCore:GetMemoryManager():GetAutoFollow():SetIsAutoRunning(0)
	elseif subcommand == 'alltarget' then
		local target = get_target()
		if target == nil then return end
		AshitaCore:GetChatManager():QueueCommand(1, '/multisend send /sendalltarget target ' .. tostring(target.id))
	elseif subcommand == 'youtarget' then
        if subcommand2 == nil then return end
		local target = get_target()
		if target == nil then return end
		AshitaCore:GetChatManager():QueueCommand(1, '/multisend sendto '..subcommand2..' /sendalltarget target ' .. tostring(target.id))
	elseif subcommand == 'allattack' then
		local target = get_target()
		if target == nil then return end
		AshitaCore:GetChatManager():QueueCommand(1, '/multisend send /sendalltarget attack ' .. tostring(target.id))
	elseif subcommand == 'youattack' then
        if subcommand2 == nil then return end
		local target = get_target()
		if target == nil then return end
		AshitaCore:GetChatManager():QueueCommand(1, '/multisend sendto '..subcommand2..' /sendalltarget attack ' .. tostring(target.id))
	elseif subcommand == 'allswitch' then
		local target = get_target()
		if target == nil then return end
		AshitaCore:GetChatManager():QueueCommand(1, '/multisend send/ sendalltarget swittch ' .. tostring(target.id))
	elseif subcommand == 'youswitch' then
        if subcommand2 == nil then return end
		local target = get_target()
		if target == nil then return end
		AshitaCore:GetChatManager():QueueCommand(1, '/multisend sendto '..subcommand2..' /sendalltarget switch ' .. tostring(target.id))
	elseif subcommand == 'attack' or subcommand == 'switch' then
        if subcommand2 == nil then return end
		local id = tonumber(subcommand2)
		local target = get_target_by_id(id)
		local category
		if subcommand == 'attack' then
			category = 0x02
		elseif subcommand == 'switch' then
			category = 0x0F
		else
			return
		end
		if target and target.valid_target and target.spawn_type == 16 and math.sqrt(target.distance) <= 30 then
            local p = struct.pack("bbbbI4HHHH", 0x01A, 0x0E, 0x00, 0x00, target.id, target.index, category, 0x00, 0x00):totable()
            AshitaCore:GetPacketManager():AddOutgoingPacket(0x01A, p)
		end
	elseif subcommand == 'target' then
        if subcommand2 == nil then return end
		local id = tonumber(subcommand2)
		local target = get_target_by_id(id)
		if not target then
			return
		end
		local player = get_player()
        if player == nil then return end
        local p = struct.pack("bbbbI4I4H", 0x058, 0x0A, 0x00, 0x00, player.id, target.id, player.index):totable()
        AshitaCore:GetPacketManager():AddIncomingPacket(0x058, p)
	elseif subcommand == 'allcommand' then
        if subcommand2 == nil then return end
		local message = args:concat(' ')
		local mobid = get_target()
		if target == nil then return end
		if mobid and mobid.id then
			if message == nil then
				AshitaCore:GetChatManager():QueueCommand(1, '/multisend send '..subcommand2..' '..mobid.id..'')
			elseif subcommand2 then
				AshitaCore:GetChatManager():QueueCommand(1, '/multisend send '..subcommand2..' '..message..' '..mobid.id..'')
			end
		end
	elseif subcommand == 'youcommand' then
        if subcommand2 == nil then return end
		local message = args:concat(' ')
		local mobid = get_target()
		if target == nil then return end
		if mobid and mobid.id then
			AshitaCore:GetChatManager():QueueCommand(1, '/multisend sendto '..subcommand2..' '..message..' '..mobid.id..'')
		end
    end
end)
